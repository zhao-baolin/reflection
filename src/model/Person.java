package model;

/**
 * @Description
 * @Author zhaobaolin
 * @Date 2018/10/20
 */
public class Person {
    private Long idCard;

    public Integer parentId;

    public Person() {
    }

    public Person(Long idCard) {
        this.idCard = idCard;
    }

    public Long getIdCard() {
        return idCard;
    }

    public void setIdCard(Long idCard) {
        this.idCard = idCard;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @Override
    public String toString() {
        return "Person{" +
                "idCard=" + idCard +
                '}';
    }
}
