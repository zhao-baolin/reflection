package model;

import java.beans.Transient;
import java.math.BigDecimal;

/**
 * @Description
 * @Author zhaobaolin
 * @Date 2018/10/20
 */
@Deprecated
public class User extends Person{
    private Long id;
    private String name = "默认名称";
    private BigDecimal amount;
    static String sex = "女";
    public String a = "测试";

    public User() {
    }

    public User(Long id) {
        this.id = id;
    }

    private User(Long id,String name){
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public static String getSex() {
        return sex;
    }

    public static void setSex(String sex) {
        User.sex = sex;
    }

    public String getA() {
        return a;
    }

    @Transient
    public void setA(String a) {
        this.a = a;
    }


    @Deprecated
    private int dd(int d){
        return d + 2;
    }

    /**
     * 注释内容
     * @return void
     */
    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", amount=" + amount +
                "} " + super.toString();
    }
}
